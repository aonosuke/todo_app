require 'rails_helper'

describe UsersController do
    describe 'GET #new' do
        context 'ページにユーザがアクセスした時' do
            before { get :new }
            
            it '正常に動作しているか (http status)' do
                expect(response.status).to eq(200)
            end
            
            it '正常にHTTPメソッドを呼び出せているか (render template)' do
                expect(response).to render_template :new
            end
        end
    end
    
    describe 'POST #create' do
        context '有効なユーザー登録に対するテスト' do
            before do
                @pre_user_count = User.count
                post :create, params: { user: { name:  "Example User",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
            end
            
            it '正常に動作しているか (http status)' do
                expect(response.status).to eq(302)
            end
            
            it 'リダイレクト (redirect)' do
                expect(response).to redirect_to user_path(1)
            end
        end
        
        context '無効なユーザー登録に対するテスト' do
            before do
                @pre_user_count = User.count
                post :create, params: { user: { name:  "",
                                         email: "user@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
            end
            
            it '正常に動作しているか (http status)' do
                expect(response.status).to eq(200)
            end
            
            it '正常にHTTPメソッドを呼び出せているか (render template)' do
                expect(response).to render_template :new
            end
        end
    end
    
    describe 'GET #index' do
        context 'ページにユーザがアクセスした時' do
            before { get :index }
            
            it '正常に動作しているか (http status)' do
                expect(response.status).to eq(302)
            end
            
            it 'リダイレクト (redirect)' do
                expect(response).to redirect_to login_url
            end
        end
    end
    
    describe 'Delete #destroy' do
        context 'ログインせずに削除しようとした時' do
            before do
                User.create(name:  "Example User",
                            email: "user@example.com",
                            password:              "password",
                            password_confirmation: "password",
                            admin: true)
                User.create(name:  "Example User",
                            email: "user@example.com",
                            password:              "password",
                            password_confirmation: "password",
                            admin: false)
                @pre_user_count = User.count
                delete :destroy, params: { id: 2 }
            end
            
            it '正常に動作しているか (http status)' do
                expect(response.status).to eq(302)
            end
            
            it 'リダイレクト (redirect)' do
                expect(response).to redirect_to login_url
            end
            
            it 'ユーザが削除されていないか' do
                expect(User.count).to eq(@pre_user_count)
            end
        end
        
        context '管理者権限がない場合' do
            before do
                User.create(name:  "Example User",
                            email: "user@example.com",
                            password:              "password",
                            password_confirmation: "password",
                            admin: true)
                post :create, params: { user: { name:  "Example User2",
                                                email: "user2@example.com",
                                                password:              "password",
                                                password_confirmation: "password" } }
                @pre_user_count = User.count
                delete :destroy, params: { id: 1 }
            end
            
            it '正常に動作しているか (http status)' do
                expect(response.status).to eq(302)
            end
            
            it 'リダイレクト (redirect)' do
                expect(response).to redirect_to root_url
            end
            
            it 'ユーザが削除されていないか' do
                expect(User.count).to eq(@pre_user_count)
            end
        end
    end
end
