require 'rails_helper'

describe SessionsController do
  describe 'GET #new' do
        context 'ページにユーザがアクセスした時' do
            before { get :new }
            
            it '正常に動作しているか (http status)' do
                expect(response.status).to eq(200)
            end
            
            it '正常にHTTPメソッドを呼び出せているか (render template)' do
                expect(response).to render_template :new
            end
        end
    end
end
