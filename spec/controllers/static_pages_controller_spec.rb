require 'rails_helper'

describe StaticPagesController do
    describe 'GET #home' do
        context 'ページにユーザがアクセスした時' do
            before { get :home }
            
            it '正常に動作しているか (http status)' do
                expect(response.status).to eq(200)
            end
            
            it '正常にHTTPメソッドを呼び出せているか (render template)' do
                expect(response).to render_template :home
            end
        end
    end
end
