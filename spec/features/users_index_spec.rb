require 'rails_helper'

describe 'ユーザ一覧表示テスト' do
    context '表示に成功した時' do
        before do 
            @admin = User.create(name:  "Example User",
                                 email: "user@example.com",
                                 password:              "password",
                                 password_confirmation: "password",
                                 admin: true)

            30.times do |n|
                User.create(name:  "User #{n}",
                            email: "user-#{n}@example.com",
                            password:              "password",
                            password_confirmation: "password")
            end
        end
        it 'ページネーションを含めたUsersIndexのテスト' do
            visit login_path
            fill_in 'Email',    with: 'user@example.com'
            fill_in 'Password', with: 'password'
            click_button 'Log in'
            click_link 'Users'
            User.paginate(page: 1).each do |user|
                expect(page).to have_link user.name
                unless user == @admin
                    expect(page).to have_link 'delete'
                end
            end
            pre_user_count = User.count
            delete_user = User.find_by(email: 'user-1@example.com')
            click_link 'delete', href: user_path(delete_user)
            expect(User.count).not_to eq(pre_user_count)
        end
    end
end