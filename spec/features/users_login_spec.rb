require 'rails_helper'

describe 'ユーザログインテスト' do
    context '正しい情報でログインした時' do
        before { User.create(name:  "Example User",
                             email: "user@example.com",
                             password:              "password",
                             password_confirmation: "password") }
        it 'ログイン→ログアウト' do
            visit login_path
            fill_in 'Email',    with: 'user@example.com'
            fill_in 'Password', with: 'password'
            click_button 'Log in'
            expect(page).not_to have_link 'Log in'
            expect(page).not_to have_link 'Sign up'
            expect(page).to have_link 'Users'
            expect(page).to have_link 'Profile'
            expect(page).to have_link 'Settings'
            expect(page).to have_link 'Log out'
            click_link 'Log out'
            expect(page).to have_link 'Log in'
            expect(page).to have_link 'Sign up'
            expect(page).not_to have_link 'Users'
            expect(page).not_to have_link 'Profile'
            expect(page).not_to have_link 'Settings'
            expect(page).not_to have_link 'Log out'
        end
    end
    
    context '正しくない情報でログインした時' do
        context 'ログイン失敗後、ホーム画面へ戻った時' do
            it 'フラッシュメッセージが消えていること' do
                visit login_path
                fill_in 'Email',    with: ''
                fill_in 'Password', with: ''
                click_button 'Log in'
                expect(page).to have_content 'Invalid email/password combination'
                visit root_path
                expect(page).not_to have_content 'Invalid email/password combination'
            end
        end
    end
end