require 'rails_helper'

describe 'ユーザ情報編集テスト' do
    context '編集に成功した時' do
        before { User.create(name:  "Example User",
                             email: "user@example.com",
                             password:              "password",
                             password_confirmation: "password") }
        it '編集ページがそのまま表示されているか' do
            visit login_path
            fill_in 'Email',    with: 'user@example.com'
            fill_in 'Password', with: 'password'
            click_button 'Log in'
            click_link 'Settings'
            fill_in 'Name',         with: "Foo Bar"
            fill_in 'Email',        with: "foo@bar.com"
            click_button 'Save changes'
            expect(page).to have_content 'Profile updated'
        end
    end
    
    context '編集に失敗した時' do
        before { User.create(name:  "Example User",
                             email: "user@example.com",
                             password:              "password",
                             password_confirmation: "password") }
        it '編集ページがそのまま表示されているか' do
            visit login_path
            fill_in 'Email',    with: 'user@example.com'
            fill_in 'Password', with: 'password'
            click_button 'Log in'
            click_link 'Settings'
            fill_in 'Name',         with: ''
            fill_in 'Email',        with: 'user@invalid'
            fill_in 'Password',     with: 'foo'
            fill_in 'Confirmation', with: 'bar'
            click_button 'Save changes'
            expect(page).to have_content 'Update your profile'
        end
    end
    
    context '別のユーザの編集ページにアクセスした場合' do
        before do 
            @user = User.create(name:  "Example User",
                                email: "user@example.com",
                                password:              "password",
                                password_confirmation: "password")
            @other_user = User.create(name:  "Sterling Archer",
                                      email: "duchess@example.gov",
                                      password:              "password",
                                      password_confirmation: "password")
        end
        
        it 'トップページに誘導されるか' do
            visit login_path
            fill_in 'Email',    with: 'duchess@example.gov'
            fill_in 'Password', with: 'password'
            click_button 'Log in'
            visit edit_user_path(@user)
            expect(page).to have_content 'Welcome to the ToDo Manager'
        end
    end
    
    context 'ログインせずに編集ページにアクセスした場合' do
        before do 
            @user = User.create(name:  "Example User",
                                email: "user@example.com",
                                password:              "password",
                                password_confirmation: "password")
        end
        
        it 'トップページに誘導されるか' do
            visit edit_user_path(@user)
            expect(page).to have_content 'Please log in.'
            expect(page).to have_button  'Log in'
        end
        
        it 'フレンドリーフォワーディングか' do
            visit edit_user_path(@user)
            fill_in 'Email',    with: 'user@example.com'
            fill_in 'Password', with: 'password'
            click_button 'Log in'
            expect(page).to have_field 'Name', with: 'Example User'
            expect(page).to have_field 'Email', with: 'user@example.com'
        end
    end
end