require 'rails_helper'

describe 'ユーザ登録テスト' do
    context '正しい情報で登録した時' do
        it '正しく登録できているか(テーブルに登録されたか/登録後ログイン状態か)' do
            pre_user_count = User.count
            visit signup_path
            fill_in 'Name',         with: 'Example User'
            fill_in 'Email',        with: 'user@example.com'
            fill_in 'Password',     with: 'password'
            fill_in 'Confirmation', with: 'password'
            click_button 'Create my account'
            expect(User.count).to be > pre_user_count
            expect(page).not_to have_link 'Log in'
        end
    end

    context '正しくない情報で登録した時' do
        it 'ユーザが登録できていないか(テーブルに登録されていないか)' do
            pre_user_count = User.count
            visit signup_path
            fill_in 'Name',         with: ''
            fill_in 'Email',        with: 'user@invalid'
            fill_in 'Password',     with: 'foo'
            fill_in 'Confirmation', with: 'bar'
            click_button 'Create my account'
            expect(User.count).to eq(pre_user_count)
        end
    end
end