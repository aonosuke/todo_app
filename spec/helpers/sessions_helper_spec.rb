require 'rails_helper'

describe SessionsHelper do
    context '永続的セッションのテスト' do
        before do
            @user = User.create(name:  "Example User",
                                email: "user@example.com",
                                password:              "password",
                                password_confirmation: "password")
            remember(@user)
        end
        
        it 'current_user returns right user when session is nil' do
            expect(@user).to eq(current_user)
            expect(!session[:user_id].nil?).to be_truthy
        end
        
        it 'current_user returns nil when remember digest is wrong' do
            @user.update_attribute(:remember_digest, User.digest(User.new_token))
            expect(current_user.nil?).to be_truthy
        end
    end
end
